import processing.serial.*;
Serial myPort;   
PFont font, fontB;
int page=0,press;
//---- define some frequently used colors -----
color red = color(255, 0, 0);
color green= color(0, 255, 0);
color blue= color(0, 0, 255);
//----------------------------------------------

//------ serial variables -------
int rx_temp,rx_pt;
int rx_value[]=new int[9];
//-------------------------------

//----- pid gain values ---------
float pid_gain_vl[]=new float[18];
//-------------------------------

//---- define string values -------------------
String pid_text[]=new String[20];
int pid_txt_x_offset=20;
int pid_txt_y_offset=100;
void string_setup()
{
  //------ x axis --------
  pid_text[0]="P_GX_GAIN";// p_gyro_x_gain
  pid_text[1]="I_GX_GAIN";
  pid_text[2]="D_GX_GAIN";

  pid_text[3]="P_IX_GAIN";// p_imu_x_gain
  pid_text[4]="I_IX_GAIN";
  pid_text[5]="D_IX_GAIN";
  //----------------------

  //------ y axis --------
  pid_text[6]="P_GY_GAIN";// p_gyro_y_gain
  pid_text[7]="I_GY_GAIN";
  pid_text[8]="D_GY_GAIN";

  pid_text[9]="P_IY_GAIN";// p_imu_y_gain
  pid_text[10]="I_IY_GAIN";
  pid_text[11]="D_IY_GAIN";
  //----------------------

  //------ z axis --------
  pid_text[12]="P_GZ_GAIN";// p_gyro_z_gain
  pid_text[13]="I_GZ_GAIN";
  pid_text[14]="D_GZ_GAIN";

  pid_text[15]="Trim_value_x_axis";
  pid_text[16]="Trim_value_y_axis";
  pid_text[17]="UNUSED";
  //----------------------
}
//---------------------------------------------

void setup()
{
  size(400, 300);
  background(0);
  //------ font -------
  font=loadFont("TimesNewRomanPSMT-48.vlw");
  fontB=loadFont("TimesNewRomanPS-BoldMT-48.vlw");
  //-------------------

  //---- configure seria port -----
  println(Serial.list());
  myPort = new Serial(this,"COM18", 115200);
  //-------------------------------
  //pid_gain_setup();
  string_setup();//load pid_txt variables
}

void draw()
{
  clear();

  draw_mouse_pos();
  draw_title();
  draw_pid_txt();
  change_pid_value();
  draw_pid_gain_values();
  draw_move_box();
  draw_sending_box();
  draw_reading_box();
}

void draw_mouse_pos()
{
  textFont(font, 15);
  fill(green);
  text(mouseX+" "+mouseY, 10, 300);
}

void draw_title()
{
  textFont(fontB, 20);
  fill(green);
  text("AIR PID TUNNING", 110, 30);
}

void draw_pid_txt()
{
  int pt;
  textFont(font, 20);
  for (int i=0;i<3;i++)
  {
    pt=3*page+i;
    text(pid_text[pt], pid_txt_x_offset, pid_txt_y_offset+60*i);
  }
}

void draw_pid_gain_values()
{
  int pt;
  noFill();  
  for (int i=0;i<3;i++)
  {
    stroke(#716A6A);
    strokeWeight(1);
    rect(155, 84+60*i, 20, 20);
    rect(265, 84+60*i, 20, 20);

    stroke(red);
    strokeWeight(3);
    line(160, 95+60*i, 170, 95+60*i);
    line(270, 95+60*i, 280, 95+60*i);
    line(275, 90+i*60, 275, 100+60*i);

    strokeWeight(3);
    stroke(blue);
    rect(180, 70+60*i, 80, 40);

    pt=3*page+i;
    fill(green);
    textFont(font,20);
    text(pid_gain_vl[pt],182,100+60*i);
    noFill();
    }
  }
  
  void draw_move_box()
  {
    textFont(font,15);
    fill(red);
    text("PRV",170,270);   
    text("NXT",230,270);
    noFill();
    
    stroke(#716A6A);
    strokeWeight(3);
     if(press==1)
    {
      fill(blue);
    }
    else noFill();
    rect(160,250,50,25);
    
     if(press==2)
    {
      fill(blue);
    }
    else noFill();
    rect(220,250,50,25);
    
  }

void draw_sending_box()
{
  stroke(#716A6A);
  strokeWeight(3);
  if(press==3)
  {
    fill(blue);
  }
  else noFill();
  rect(325,135,60,40);
  
  fill(red);
  textFont(fontB,18);
  text("SEND",330,165);
}

void draw_reading_box()
{
  stroke(#716A6A);
  strokeWeight(3);
  if(press==4)
  {
    fill(blue);
  }
  else noFill();
  rect(325,205,60,40);
  
  fill(red);
  textFont(fontB,18);
  text("READ",330,235);
}

void change_pid_value()//high speed
{
  if (mousePressed==true && mouseButton==LEFT)
  {
    for(int i=0;i<3;i++)
  {
    if(mouseX>238 && mouseX<260 && mouseY>(83+60*i) && mouseY<(105+60*i) )
    {
      pid_gain_vl[page*3+i]+=0.001;
      if(pid_gain_vl[page*3+i]>100)pid_gain_vl[page*3+i]=100;
    }
  }
  
  for(int i=0;i<3;i++)
  {
    if(mouseX>180 && mouseX<222 && mouseY>(83+60*i) && mouseY<(105+60*i) )
    {
      pid_gain_vl[page*3+i]-=0.001;
      if(pid_gain_vl[page*3+i]<0)pid_gain_vl[page*3+i]=0;
    }
  }
  
  for(int i=0;i<3;i++)
  {
    if(mouseX>289 && mouseX<315 && mouseY>(83+60*i) && mouseY<(105+60*i) )
    {
      pid_gain_vl[page*3+i]+=0.02;
      if(pid_gain_vl[page*3+i]>100)pid_gain_vl[page*3+i]=100;
    }
  }
  
  for(int i=0;i<3;i++)
  {
    if(mouseX>127 && mouseX<150 && mouseY>(83+60*i) && mouseY<(105+60*i) )
    {
      pid_gain_vl[page*3+i]-=0.02;
      if(pid_gain_vl[page*3+i]<0)pid_gain_vl[page*3+i]=0;
    }
  }
  }
}

void send_data()
{
  int temp_data[]=new int[9];
  int temp_gain_vl;
  
  for(int i=0;i<3;i++)
  {
    temp_gain_vl=(int)(pid_gain_vl[page*3+i]*1000);
    temp_data[3*i]=temp_gain_vl/10000;
    temp_data[3*i+1]=(temp_gain_vl/100)%100;
    temp_data[3*i+2]=temp_gain_vl % 100;
  }
  
  //-------send data and debug ------------
  myPort.write(200+page);//send request
  for(int i=0;i<9;i++)
  {
     //print(temp_data[i]+ " ");
     myPort.write(temp_data[i]);
  }
  //println(" ");
  //-------------------------
}

void read_pid_value()
{
  for(int i=0;i<3;i++)
  {
  pid_gain_vl[3*page+i]=(float)((rx_value[3*i]*1e4+rx_value[3*i+1]*1e2+rx_value[3*i+2])/1000);
  }
}

void mousePressed()
{
  if(mouseButton==LEFT)
  {
  if(mouseX<211 && mouseX >159 && mouseY>250 && mouseY<275)//previouse page
  {
    press=1;
    page--;
    if(page<0)page=0;
  }
  else if(mouseX<271 && mouseX >220 && mouseY>250 && mouseY<275)//next page
  {
    press=2;
    page++;
    if(page>5)page=5;
  }
  
  else if(mouseX<385 && mouseX >324 && mouseY>137 && mouseY<176)//send button
  {
    press=3;
    send_data();
  }
  
  else if(mouseX<385 && mouseX >324 && mouseY>205 && mouseY<245)
  {
    press=4;
    myPort.write(210+page);
  }
  
  for(int i=0;i<3;i++)
  {
    if(mouseX>265 && mouseX<285 && mouseY>(83+60*i) && mouseY<(105+60*i) )// increase pid value with low speed
    {
      pid_gain_vl[page*3+i]+=0.001;
      if(pid_gain_vl[page*3+i]>100)pid_gain_vl[page*3+i]=100;
    }
  }
  
  for(int i=0;i<3;i++)
  {
    if(mouseX>154 && mouseX<175 && mouseY>(83+60*i) && mouseY<(105+60*i) )// increase pid value with high speed
    {
      pid_gain_vl[page*3+i]-=0.001;
      if(pid_gain_vl[page*3+i]<0)pid_gain_vl[page*3+i]=0;
    }
  }  
 }
}

void mouseReleased()
{
  press=0;
}

void serialEvent(Serial port) 
{
  rx_temp=myPort.read();
  if(rx_temp==255)
  {
  rx_pt=0;
  }
  else
  {
    rx_value[rx_pt]=rx_temp;
    rx_pt++;
  }
  if(rx_pt>8)//receive complete
  {
    read_pid_value();
  }
}
