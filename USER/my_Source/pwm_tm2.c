#include "stm32f10x.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_tim.h"
#include "pwm_tm2.h"


void TM2_PWM_Configuration(void)
{
	// Setup PWM by using timer2
	// T=2.5ms
	// t_on=0-2.5ms
	// 4 pwm chanels: pa0(c1),pa0(c2),pa0(c3),pa0(c4)
	TIM_TimeBaseInitTypeDef TM2_TB_Config;
	TIM_OCInitTypeDef TM2_OC_Config;
	GPIO_InitTypeDef GPIO_PWM_CHN_Config;
	
	//--- enable clock source for timer2, gpioa and afio------
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA|RCC_APB2Periph_AFIO,ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2,ENABLE);
	//------------------------++++++++++++++++++---------------------
	
	//--- setup pa[0-3] as pwm outputs ------------
	GPIO_PWM_CHN_Config.GPIO_Mode=GPIO_Mode_AF_PP;
	GPIO_PWM_CHN_Config.GPIO_Pin=GPIO_Pin_0|GPIO_Pin_1|GPIO_Pin_2|GPIO_Pin_3;
	GPIO_PWM_CHN_Config.GPIO_Speed=GPIO_Speed_50MHz;
	GPIO_Init(GPIOA,&GPIO_PWM_CHN_Config);
	//---------------+++++++++++-------------------
	
	//------------ setup timer2 -------------------
	//first, setup timebase structure
	TM2_TB_Config.TIM_ClockDivision=TIM_CKD_DIV1 ;
	TM2_TB_Config.TIM_CounterMode=TIM_CounterMode_Up;
	TM2_TB_Config.TIM_Period=5000;//T=Period*t_step=2.5ms
	TM2_TB_Config.TIM_Prescaler=36-1;//t_step=(Prescaler+1)/core_clock=0.5us
	TIM_TimeBaseInit(TIM2,&TM2_TB_Config);
	//second, setup output compare structure
	TM2_OC_Config.TIM_OCMode=TIM_OCMode_PWM1;
	TM2_OC_Config.TIM_OCPolarity=TIM_OCPolarity_High;
	TM2_OC_Config.TIM_OutputState=TIM_OutputState_Enable ;
	TM2_OC_Config.TIM_Pulse=0;
	TIM_OC1Init(TIM2,&TM2_OC_Config);
	TIM_OC2Init(TIM2,&TM2_OC_Config);
	TIM_OC3Init(TIM2,&TM2_OC_Config);
	TIM_OC4Init(TIM2,&TM2_OC_Config);
	
	//enable auto reload for OC_reg and ARR_reg
	TIM_ARRPreloadConfig(TIM2,ENABLE);//can change T 
	TIM_OC1PreloadConfig(TIM2,ENABLE);//can change t_on
	TIM_OC2PreloadConfig(TIM2,ENABLE);//can change t_on
	TIM_OC3PreloadConfig(TIM2,ENABLE);//can change t_on
	TIM_OC4PreloadConfig(TIM2,ENABLE);//can change t_on
	TIM_Cmd(TIM2,ENABLE);
	//---------------+++++++-----------------------
}

void Motor_Disabled(void)
{
	TIM2->CCR1=0;
	TIM2->CCR2=0;
	TIM2->CCR3=0;
	TIM2->CCR4=0;
}
void Motor_init(void)
{
	TIM2->CCR1=STOP_PWM;
	TIM2->CCR2=STOP_PWM;
	TIM2->CCR3=STOP_PWM;
	TIM2->CCR4=STOP_PWM;
}

/*
void esc_calib(void)
{
	TIM2->CCR1=MAX_PWM;
	TIM2->CCR2=MAX_PWM;
	TIM2->CCR3=MAX_PWM;
	TIM2->CCR4=MAX_PWM;
	delay_ms(2000);
	TIM2->CCR1=MIN_PWM;
	TIM2->CCR2=MIN_PWM;
	TIM2->CCR3=MIN_PWM;
	TIM2->CCR4=MIN_PWM;
	delay_ms(2000);
}
*/
