#include "stm32f10x.h"
#include <stdio.h>
#include "usart1.h"

uint16_t rx1_temp;
uint8_t port=1;//use usart1 by default

#ifdef __GNUC__
  /* With GCC/RAISONANCE, small printf (option LD Linker->Libraries->Small printf
     set to 'Yes') calls __io_putchar() */
  #define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
#else
  #define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
#endif /* __GNUC__ */


void USART1_Configuration(void)
{ 
  GPIO_InitTypeDef GPIO_InitStructure;// define object
  USART_InitTypeDef USART_InitStructure; 

	//enable clock for usart_pin: PA9 ^ PA10, beside we have to provide clk source for USART component
	//arcording to datasheet: porta and usart1 uses APB2
  RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOA | RCC_APB2Periph_USART1,ENABLE);
				
	//--- config tx pin PA9
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;	         
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;// can't change to GPIO_Mode_Out_PP
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; 
  GPIO_Init(GPIOA, &GPIO_InitStructure);//apply configuration for PINA9		

  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;	        
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;  
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; 
  GPIO_Init(GPIOA, &GPIO_InitStructure);//apply configuration for PINA10

  USART_InitStructure.USART_BaudRate = 115200;
  USART_InitStructure.USART_WordLength = USART_WordLength_8b;
  USART_InitStructure.USART_StopBits = USART_StopBits_1;
  USART_InitStructure.USART_Parity = USART_Parity_No;
  USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
  USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
  USART_Init(USART1, &USART_InitStructure);//apply change for usart1 
	
  USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);//enable intr when recived complete
  //USART_ITConfig(USART1, USART_IT_TXE, ENABLE);//enable intr when a transmittion complete
  USART_ClearFlag(USART1,USART_FLAG_TC);//clear this flag to prepare a new transmittion
  USART_Cmd(USART1, ENABLE);//enable usart1
}

//------ nvic setupt for usart1 intr ----------
void USART1_NvicSetup(void)//enable global USAR1_intr
{
	NVIC_InitTypeDef NVIC_InitStructure; 

  NVIC_PriorityGroupConfig(NVIC_PriorityGroup_0);  												
  NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn ;// 
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;   
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;	      
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);//apply setting for usart1_global_intr
}
//-----------------------------------------------
/*
//-----usart1 intr service prg-------------
void USART1_IRQHandler(void)//when we have an interrupt on USART
{
	 if((USART_GetITStatus(USART1,USART_IT_RXNE))!=RESET)//if match receive complete interrupt
	 {
		 USART_ClearITPendingBit(USART1,USART_IT_RXNE);//clear the flag bit for the next time
		 //-------------------------------------		 
		 GPIOB->ODR|=0x02;
		 rx1_temp= USART_ReceiveData(USART1);	
		 //------ put intr service prg here  ---
		 //-------------------------------------
	 }
 }
//----------------+-----------------------
*/
/**
  * @brief  Retargets the C library printf function to the USART.
  * @param  None
  * @retval None
  */
PUTCHAR_PROTOTYPE
{
	switch(port)
	{
		case 1: //usart 1
			USART_SendData(USART1, (uint8_t) ch);
			while (USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET)
			{}
			break;
		case 3://usart 3
			USART_SendData(USART3, (uint8_t) ch);
			while (USART_GetFlagStatus(USART3, USART_FLAG_TC) == RESET)
			{}
			break;	
	}
  return ch;
}
