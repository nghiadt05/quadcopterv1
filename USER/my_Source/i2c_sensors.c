#include "i2c_sensors.h"

//------- define data for hmc5883l -----------------
#define I2C_SPEED 100000//HZ
#define hmc_5883l_wadr 0x3c
#define hmc_5883l_radr 0x3d

#define cofig_reg_a 0x00
#define cofig_reg_b 0x01
#define mode_reg 0x02
#define hmc5883l_X_MSB 0x03//start readding addres

#define hmc5883_gain 1090 // LSB/Gauss
//#define declinationAngle -10.18/1000//radian, right only in Hanoi


#define calib
#define raw_x_offset -117
#define raw_y_offset -167
#define raw_z_offset -66
#define scale_x 0.94200543 //important data
#define scale_y 0.9367283 	
#define scale_z 0.88357495

//------------end of hmc5883l-----------------------

//------- define data for MPU6050 setup ------------
/*
#define   ACCEL_XOUT_H	59	// R  ACCEL_XOUT[15:8] 
#define   ACCEL_XOUT_L	60	// R  ACCEL_XOUT[7:0] 
#define   ACCEL_YOUT_H	61	// R  ACCEL_YOUT[15:8] 
#define   ACCEL_YOUT_L	62	// R  ACCEL_YOUT[7:0] 
#define   ACCEL_ZOUT_H	63	// R  ACCEL_ZOUT[15:8] 
#define   ACCEL_ZOUT_L	64	// R  ACCEL_ZOUT[7:0] 
#define   TEMP_OUT_H	65	// R  TEMP_OUT[15:8] 
#define   TEMP_OUT_L	66	// R  TEMP_OUT[7:0] 
#define   GYRO_XOUT_H	67	// R  GYRO_XOUT[15:8] 
#define   GYRO_XOUT_L	68	// R  GYRO_XOUT[7:0] 
#define   GYRO_YOUT_H	69	// R  GYRO_YOUT[15:8] 
#define   GYRO_YOUT_L	70	// R  GYRO_YOUT[7:0] 
#define   GYRO_ZOUT_H	71	// R  GYRO_ZOUT[15:8] 
#define   GYRO_ZOUT_L	72	// R  GYRO_ZOUT[7:0] 
*/

#define M_PI	3.14159265358979323846	/* pi */
#define MPU6050_SLA_ADR 0x68 
#define MPU6050_WADR MPU6050_SLA_ADR<<1
#define MPU6050_RADR (MPU6050_SLA_ADR<<1)|0x01
#define Res25_value 0x07	// Sample rate divider
#define Res26_value 0x00	// Configuration		    
#define Res27_value 0x00	// Gyroscope configuration: gyro_scale= +-250 degree/s; sen=131 LSB/degree/s
#define Res28_value 0x00  // Accelerometter configuration  : accelero_scale=+-2g ; sen=16384 LSB/g       
#define Res55_value 0x02	// By pass enable configuration
#define Res106_value 0x00 // user control
#define Res107_value 0x00 // do not reset all the device //0x80

#define Res25 25 //register address
#define Res26 26
#define Res27 27
#define Res28 28
#define Res55 55
#define Res106 106
#define Res107 107

#define start_read_address 59
#define gyro_zero_x -55
#define gyro_zero_y -31
#define gyro_zero_z -123

#define gyro_sensitivity 131
#define acce_sensitivity 16384
#define acc_x_gain 16418
#define acc_y_gain 16403
#define acc_z_gain 16600

//---------------- end of data for MPU 6050 ---------

//--------- variables for mpu6050 ----------
int16_t gyro_x,gyro_y,gyro_z;
int16_t acc_x,acc_y,acc_z;
float last_gyro_x_rate,last_gyro_y_rate,last_gyro_z_rate;
float gyro_x_rate,gyro_y_rate,gyro_z_rate;
float acc_x_angle,acc_y_angle;
float acc_x_temp,acc_y_temp,acc_z_temp;
float dt,temp;
uint8_t i2c_sdata[20],i2c_rdata[20];
//------------------------------------------

//--------- variables for hmc5883l ---------
uint8_t hmc5883_wdata[3];
uint8_t hmc5883_rdata[6];
float raw_x,raw_y,raw_z;
int16_t raw_x_temp,raw_y_temp,raw_z_temp;
uint16_t mp;//mag_pointer

float mag_x,mag_y,mag_z;
float heading,heading_temp,headingDegrees;

//float heading2,heading2Degrees;
float x_h,y_h;//for tilt process
//------------------------------------------

void I2C_Configuration(void)
{
	GPIO_InitTypeDef GPIO_I2C_Config;
	I2C_InitTypeDef I2C_Config;
	
	//--enable clock source for i2c and gpiob --
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C1, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
	//------------------------------------------
	
	//-- configuration pinb6 and pinb7 as out_put_od --
	GPIO_I2C_Config.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7;
	GPIO_I2C_Config.GPIO_Speed = GPIO_Speed_10MHz; 
	GPIO_I2C_Config.GPIO_Mode = GPIO_Mode_AF_OD; 
	GPIO_Init(GPIOB, &GPIO_I2C_Config);
	//-------------------------------------------------
	
	//-- configuration i2c ----------------------------
	I2C_Config.I2C_Ack=I2C_Ack_Enable;
	I2C_Config.I2C_AcknowledgedAddress=I2C_AcknowledgedAddress_7bit;
	I2C_Config.I2C_ClockSpeed=I2C_SPEED;
	I2C_Config.I2C_DutyCycle=I2C_DutyCycle_2;
	I2C_Config.I2C_Mode=I2C_Mode_I2C;
	I2C_Config.I2C_OwnAddress1=0x01;
	I2C_Init(I2C1,&I2C_Config);
	I2C_Cmd(I2C1,ENABLE);
	//-------------------------------------------------
}

void mpu6050_write_block(uint8_t adr, uint8_t data[], uint8_t data_len)//okie
{
	uint8_t i;
	// Send START condition 
  I2C_GenerateSTART(I2C1, ENABLE);
	//loop until match the 'start condition' flag
	while(!I2C_GetFlagStatus(I2C1, I2C_FLAG_SB));
	
	// send mpu6050 addres and check until it's done
	I2C_Send7bitAddress(I2C1, MPU6050_WADR, I2C_Direction_Transmitter);
	while(!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED));
	
	// send mpu6050 the begining address where we want to send data, check until it's done
	I2C_SendData(I2C1,adr);
	while(!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_BYTE_TRANSMITTED));
	
	//then send continous data in data[] array
	for(i=0;i<data_len;i++)
	{
		I2C_SendData(I2C1,data[i]);
		while(!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_BYTE_TRANSMITTED));
		delay_ms(1);
	}
	
	// send stop condition and wait until done 
	I2C_GenerateSTOP(I2C1, ENABLE);
	while(I2C_GetFlagStatus(I2C1, I2C_FLAG_STOPF));
}

void mpu6050_read_block(uint8_t adr, uint8_t data[], uint8_t data_len)
//adr: addres of the first reg what we read from
//data[]: array of data, which we store read data in
//data_len: lenght of reading data
{
	uint8_t i;
	
	while(I2C_GetFlagStatus(I2C1, I2C_FLAG_BUSY)){};
	// Send START condition 
  I2C_GenerateSTART(I2C1, ENABLE);
	//loop until match the 'start condition' flag
	while(!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_MODE_SELECT));
	
	
	// send mpu6050 7 bit address and a Write bit, check until done
	I2C_Send7bitAddress(I2C1, MPU6050_WADR, I2C_Direction_Transmitter);
	while(!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED)){};
	//I2C_Cmd(I2C1, ENABLE);// clear all the event	
	
	
	// send mpu6050 the addres of the reg we want to read data from 
	I2C_SendData(I2C1,adr);
	while(!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_BYTE_TRANSMITTED)){};

	
	// send stop condition and wait until done 
	I2C_GenerateSTOP(I2C1, ENABLE);
	while(I2C_GetFlagStatus(I2C1, I2C_FLAG_STOPF)){};
	
	//----- send a repeat start to start reading data -----
	
	// Send START condition 
  I2C_GenerateSTART(I2C1, ENABLE);
	//loop until match the 'start condition' flag
	while(!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_MODE_SELECT));
	
	
	// send mpu6050 7 bit addres and Read command
	I2C_Send7bitAddress(I2C1, MPU6050_RADR, I2C_Direction_Receiver);
	while(!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED)){};

	
  i=0;
  while(data_len)
	{
		if(data_len==1)
		{
      I2C_AcknowledgeConfig(I2C1, DISABLE);
      I2C_GenerateSTOP(I2C1, ENABLE);
		}
		  if(I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_BYTE_RECEIVED))
    {
     
      i2c_rdata[i] = I2C_ReceiveData(I2C1);
			i++;
      data_len--;
		}
	}
	I2C_AcknowledgeConfig(I2C1, ENABLE);
}

void mpu6050_get_value(void)
{	
	mpu6050_read_block(start_read_address,i2c_rdata,14);	
	acc_x=(int16_t)(i2c_rdata[0]<<8|i2c_rdata[1]);
	acc_y=(int16_t)(i2c_rdata[2]<<8|i2c_rdata[3]);
	acc_z=(int16_t)(i2c_rdata[4]<<8|i2c_rdata[5]);
	temp=(float)((int16_t)(i2c_rdata[6]<<8|i2c_rdata[7]))/340+36.53;
	gyro_x=(int16_t)(i2c_rdata[8]<<8|i2c_rdata[9]);
	gyro_y=(int16_t)(i2c_rdata[10]<<8|i2c_rdata[11]);
	gyro_z=(int16_t)(i2c_rdata[12]<<8|i2c_rdata[13]);
}

void mpu_6050_init(void)
{
	//reset mpu6050
	delay_ms(10);
	i2c_sdata[0]=0x80;
	mpu6050_write_block(Res107,i2c_sdata,1);
	delay_ms(10);
	
	//use internal clock
	i2c_sdata[0]=Res107_value;
	mpu6050_write_block(Res107,i2c_sdata,1);
	delay_ms(10);
	
	// enable i2c master mode, arm uc will contrl the mpu6050 bypass auxilary bus
	i2c_sdata[0]=Res106_value;
	mpu6050_write_block(Res106,i2c_sdata,1);
	delay_ms(10);
	
	//enable bypass i2c bus
	i2c_sdata[0]=Res55_value;
	mpu6050_write_block(Res55,i2c_sdata,1);
	delay_ms(10);
	
	//config our meure mode, read the specific res for more info
	i2c_sdata[0]=Res25_value;
	i2c_sdata[1]=Res26_value;
	i2c_sdata[2]=Res27_value;
	i2c_sdata[3]=Res28_value;
	mpu6050_write_block(Res25,i2c_sdata,4);
	delay_ms(10);
	GPIOB->ODR|=1<<0;
}

void get_gyro_rate(void)
{
	gyro_x_rate=(float) (gyro_x-gyro_zero_x)/gyro_sensitivity;	
	gyro_y_rate=(float) (gyro_y-gyro_zero_y)/gyro_sensitivity;	
	gyro_z_rate=(float) (gyro_z-gyro_zero_z)/gyro_sensitivity;	
}

void get_acc_value(void)
{
	acc_x_temp= (float)acc_x/acc_x_gain;
	acc_y_temp= (float)acc_y/acc_y_gain;
	acc_z_temp= (float)acc_z/acc_z_gain;// [acc_z]=g=9.8 m/s.s
}
/************************************************************************/
/* change acceleron value to degree                                     */
/************************************************************************/
/*
void get_acce_angle(void)
{
	acc_x_temp= (float)acc_x/acc_x_gain;
	acc_y_temp= (float)acc_y/acc_y_gain;
	acc_z_temp= (float)acc_z/acc_z_gain;// [acc_z]=g=9.8 m/s.s
	acc_x_angle=(180/M_PI)*atan(acc_y_temp/sqrt(pow(acc_x_temp,2)+pow(acc_z_temp,2)));
	acc_y_angle=(180/M_PI)*atan(-acc_x_temp/sqrt(pow(acc_y_temp,2)+pow(acc_z_temp,2)));
	
	if(acc_z<0)//kit bi dao nguoc: dung de tinh toan goc 0~(+-180) degrees
	{
		if(acc_x_angle<0)
		{
			acc_x_angle=-180-acc_x_angle;// 0>= acc_x_angle> -180
		}
		else
		{
			acc_x_angle=180-acc_x_angle;//0 =< acc_x_angle =< 180
		}
		
		if(acc_y_angle<0)
		{
			acc_y_angle=-180-acc_y_angle;// 0>= acc_x_angle> -180
		}
		else
		{
			acc_y_angle=180-acc_y_angle;//0 =< acc_x_angle =< 180
		}
	}
}
*/

void i2c_sensors_send_prcessing_data(float x_angle, float y_angle, float z_angle)
{
	int16_t pr_sdata[9];
	uint8_t i;
	pr_sdata[0]=(int16_t)x_angle;
	pr_sdata[1]=(int16_t)y_angle;
	pr_sdata[2]=(int16_t)z_angle;
	pr_sdata[3]=gyro_x;
	pr_sdata[4]=gyro_y;
	pr_sdata[5]=gyro_z;
	pr_sdata[6]=acc_x;
	pr_sdata[7]=acc_y;
	pr_sdata[8]=acc_z;
	for(i=0;i<8;i++)
	{
		printf("%d ,",pr_sdata[i]);
		delay_us(10);
	}
	printf("%d \r",pr_sdata[8]);
}

void hmc5883_write_block(uint8_t adr,uint8_t data[],uint8_t len)
{
	uint8_t i;
	//send start condition end check until done
	I2C_GenerateSTART(I2C1,ENABLE);
	while(!I2C_CheckEvent(I2C1,I2C_EVENT_MASTER_MODE_SELECT));
	
	//send 7bit addres of hmc5883 and write bit
	I2C_Send7bitAddress(I2C1,hmc_5883l_wadr,I2C_Direction_Transmitter);
	while(!I2C_CheckEvent(I2C1,I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED));
	
	//send adress of hmc5883's register 
	I2C_SendData(I2C1,adr);
	while(!I2C_CheckEvent(I2C1,I2C_EVENT_MASTER_BYTE_TRANSMITTED));
	
	//send serveral data to hmc5883l
	for(i=0;i<len;i++)
	{
		I2C_SendData(I2C1,data[i]);
	  while(!I2C_CheckEvent(I2C1,I2C_EVENT_MASTER_BYTE_TRANSMITTED));
		delay_ms(1);
	}
	
	//send stop condition 
	I2C_GenerateSTOP(I2C1,ENABLE);
	while(I2C_GetFlagStatus(I2C1, I2C_FLAG_STOPF));
	
}

void cfig_reg_a(unsigned char sample_avg, unsigned char out_put_rate, unsigned char measure_mode)
{
	//-------prepare data for register a --------
	if(sample_avg>3){sample_avg=3;}
	if(out_put_rate>7){out_put_rate=7;}
	if(measure_mode>3){measure_mode=3;}
	hmc5883_wdata[0]=(sample_avg<<5)|(out_put_rate<<2)|measure_mode;// configure register A
 	//-------------------------------------------

	//------- write config data to reg a ---------
	hmc5883_write_block(cofig_reg_a,hmc5883_wdata,1);
  //--------------------------------------------
	delay_ms(10);
}

void cfig_reg_b(unsigned char gain)
{
	//------- prepare data for register b -----
	if(gain>7)gain=7;
	hmc5883_wdata[0]=gain<<5;
	//-----------------------------------------
	
	//------- write config data to reg b ---------
	hmc5883_write_block(cofig_reg_b,hmc5883_wdata,1);
  //--------------------------------------------
	delay_ms(10);
}

void cfig_mode_reg(unsigned char mode)
{
	//------ prepare data to mode register -----
	if(mode>3)mode=3;
	hmc5883_wdata[0]=mode;	
	//------------------------------------------
	//------- write config data to mode reg ---------
	hmc5883_write_block(mode_reg,hmc5883_wdata,1);
  //--------------------------------------------
	delay_ms(10);
}

void hmc5883_init(void)
{
	cfig_reg_a(3,3,0);// output value average =8; out put frequency= 7.5 Hz (delay 33.34 ms); 
	cfig_reg_b(1);// Gain= 1090
	cfig_mode_reg(0);// continuos measurement
}

void hmc5883_get_data(void)
{
	uint8_t len,i;
	//wait until i2c bus is free
	while(I2C_GetFlagStatus(I2C1, I2C_FLAG_BUSY));
	// Send START condition 
  I2C_GenerateSTART(I2C1, ENABLE);
	//loop until match the 'start condition' flag
	while(!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_MODE_SELECT));
	
	//send hmc5883 7 bit address and write bit, wait until done
	I2C_Send7bitAddress(I2C1,hmc_5883l_wadr,I2C_Direction_Transmitter);
	while(!I2C_CheckEvent(I2C1,I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED));
	
	// send hmc5883 the begining address of the first data register
	I2C_SendData(I2C1,hmc5883l_X_MSB);
	while(!I2C_CheckEvent(I2C1,I2C_EVENT_MASTER_BYTE_TRANSMITTED));
	
	/*
	//send stop condition 
	I2C_GenerateSTOP(I2C1,ENABLE);
	while(I2C_GetFlagStatus(I2C1, I2C_FLAG_STOPF));
	*/
	
	//send a start condition: repeat start
	I2C_GenerateSTART(I2C1,ENABLE);
	while(!I2C_CheckEvent(I2C1,I2C_EVENT_MASTER_MODE_SELECT));
	
	//send hmc5883 7 bit addres and a Read bit, check until done
	I2C_Send7bitAddress(I2C1,hmc_5883l_radr,I2C_Direction_Receiver);
	while(!I2C_CheckEvent(I2C1,I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED));
	//---pass
	
	i=0;
	len=6;
	while(len)
	{
		if(len==1)//received done
		{
			I2C_AcknowledgeConfig(I2C1,DISABLE);// do not send ack, have to send notack
			I2C_GenerateSTOP(I2C1,ENABLE);// call a stop condition
			while(I2C_GetFlagStatus(I2C1, I2C_FLAG_STOPF));// wait until stop done
		}
		// received not done yet
		if(I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_BYTE_RECEIVED))
		{
			hmc5883_rdata[i]=I2C_ReceiveData(I2C1);
			i++;
			len--;
		}
	}
	I2C_AcknowledgeConfig(I2C1,ENABLE);
	
	//---- get raw data ------
	#ifdef calib// calib tu truong thong qua cac buoc sau
	raw_x_temp=(int16_t)((hmc5883_rdata[0]<<8)|hmc5883_rdata[1])-raw_x_offset;
	raw_z_temp=(int16_t)((hmc5883_rdata[2]<<8)|hmc5883_rdata[3])-raw_z_offset;
	raw_y_temp=(int16_t)((hmc5883_rdata[4]<<8)|hmc5883_rdata[5])-raw_y_offset;
		
	raw_x=(float)(raw_x_temp)/scale_x;
	raw_y=(float)(raw_y_temp)/scale_y;
	raw_z=(float)(raw_z_temp)/scale_z;
	#endif
	
	#ifndef calib
	raw_x=(int16_t)((hmc5883_rdata[0]<<8)|hmc5883_rdata[1]);
	raw_z=(int16_t)((hmc5883_rdata[2]<<8)|hmc5883_rdata[3]);
	raw_y=(int16_t)((hmc5883_rdata[4]<<8)|hmc5883_rdata[5]);
	#endif 
	
	//printf("%d %d %d\r",raw_x,raw_y,raw_z);
	mag_x=(float) raw_x/hmc5883_gain;// Gauss
	mag_y=(float) raw_y/hmc5883_gain;
	mag_z=(float) raw_z/hmc5883_gain;
	}
/*
	float hmc5883_get_heading_value(float x_angle, float y_angle)
{
	float cos_x,cos_y,sin_x,sin_y;	
	//---------------------- tilt process -----------------------------------------------------
	
	//--------- use the exiting angle-------------------
	cos_y= cos(y_angle*M_PI/180);
	sin_y= sin(y_angle*M_PI/180);
	
	cos_x= cos(x_angle*M_PI/180);
	sin_x= sin(x_angle*M_PI/180);
	//--------------------------------------------------	
	x_h= mag_x*cos_y + mag_z*sin_y;
	y_h= mag_x*sin_x*sin_y+ mag_y*cos_x-mag_z*sin_x*cos_y;
	
	//heading=(float) atan(y_h/x_h)+declinationAngle;// tru di sai so tu truong dia li
	heading=(float) atan(y_h/x_h);//khong tinh sai so tu truong dia li
	
	if(x_h<0 && y_h<0){heading-=M_PI;}
	if(x_h<0 && y_h>0){heading+=M_PI;}
		
	if(heading<-2*M_PI){heading+=2*M_PI;} 
		else if(heading>2*M_PI){heading-=2*M_PI;}
			else{}
	
	heading_temp=headingDegrees;
	headingDegrees = heading * 180/M_PI;
	
				if(headingDegrees==0)
				{
					if((heading_temp*heading_temp)>100){headingDegrees=180;}
				}
	z_angle=headingDegrees;
	return headingDegrees;	
}
*/
/*
	void hmc5883_send_processing_data(void)
	{
		printf("%f ,",headingDegrees);
		delay_us(10);
			
		printf("%d ,",(int)raw_x);
		delay_us(10);
			
		printf("%d ,",(int)raw_y);
		delay_us(10);
			
		printf("%d \r",(int)raw_z);
	}
*/
