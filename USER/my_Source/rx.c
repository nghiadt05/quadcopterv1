#include "rx.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_exti.h"
#include "stm32f10x_rcc.h"
#include "misc.h"
#include "timer1_ovf.h"
#include "math.h"
#include <stdio.h>


//---------for measuring pulse width of 6 rx chanels------ 
uint32_t start_ovf_count[6];//6 input pwm channels
uint32_t stop_ovf_count[6];
uint32_t start_count[6];
uint32_t stop_count[6];
uint32_t pulse_count[6],pulse_temp;
uint8_t control_mode;//=1: gyro mode; =0: auto balance mode
uint8_t measure_done[6]={1,1,1,1,1,1};
float rx_temp;
float rx_value[6];//store the pulse times of 6 input pwm chanels from rx device
									// rx_value[0]: target_x_angle/target_x_rate
									// rx_value[1]: target_y_angle/target_y_rate
									// rx_value[2]: throttle
									// rx_value[3]: target_z_angle/target_z_rate
									// rx_value[4]: (unused)
									// rx_value[5]: (unused)
		
//-----------++++++++++++++++++++++++++++------------------

/*
	Use PA[4:8] as interrupt inputs
	+ PA4: channel 1
	+ PA5: channel 2
	+ PA6: channel 3
	+ PA7: channel 4
	+ PA8: channel 5
	+	PA11: channel 6 not used yet
*/
void RX_Configuration(void)
{
	
	EXTI_InitTypeDef EXTI_InitStructure;
	GPIO_InitTypeDef GPIO_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;

	//--- ennable rcc for afio, gpioA and exti----
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO|RCC_APB2Periph_GPIOA,ENABLE);
	
	//--- setup these pins as input pull up ------
	GPIO_InitStructure.GPIO_Mode=GPIO_Mode_IPU;
	GPIO_InitStructure.GPIO_Pin=GPIO_Pin_4|GPIO_Pin_5|
															GPIO_Pin_6|GPIO_Pin_7|
															GPIO_Pin_8|GPIO_Pin_11;
	GPIO_InitStructure.GPIO_Speed=GPIO_Speed_50MHz;
	GPIO_Init(GPIOA,&GPIO_InitStructure);
	//--------------------------------------------
	
	//--- setup exti source for these pins -------
	GPIO_EXTILineConfig(GPIO_PortSourceGPIOA,GPIO_PinSource4);
	GPIO_EXTILineConfig(GPIO_PortSourceGPIOA,GPIO_PinSource5);
	GPIO_EXTILineConfig(GPIO_PortSourceGPIOA,GPIO_PinSource6);
	GPIO_EXTILineConfig(GPIO_PortSourceGPIOA,GPIO_PinSource7);
	GPIO_EXTILineConfig(GPIO_PortSourceGPIOA,GPIO_PinSource8);
	GPIO_EXTILineConfig(GPIO_PortSourceGPIOA,GPIO_PinSource11);
	//--------------------------------------------
	
	//--- setup exti function --------------------
	EXTI_InitStructure.EXTI_Line=EXTI_Line4|EXTI_Line5|EXTI_Line6|EXTI_Line7|EXTI_Line8|EXTI_Line11;
	EXTI_InitStructure.EXTI_LineCmd=ENABLE;
	EXTI_InitStructure.EXTI_Mode=EXTI_Mode_Interrupt;
	EXTI_InitStructure.EXTI_Trigger=EXTI_Trigger_Rising_Falling;
	EXTI_Init(&EXTI_InitStructure);//apply all setup	
	
	//clear all of pending bits for first detection
	EXTI_ClearITPendingBit(EXTI_Line4);
	EXTI_ClearITPendingBit(EXTI_Line5);
	EXTI_ClearITPendingBit(EXTI_Line6);
	EXTI_ClearITPendingBit(EXTI_Line7);
	EXTI_ClearITPendingBit(EXTI_Line8);
	EXTI_ClearITPendingBit(EXTI_Line11);
	//--------------------------------------------
	
	//---- configure nvic ------------------------
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_1);
	NVIC_InitStructure.NVIC_IRQChannel=EXTI4_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority=0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority=0;
	NVIC_InitStructure.NVIC_IRQChannelCmd=ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	
	
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_1);
	NVIC_InitStructure.NVIC_IRQChannel=EXTI9_5_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority=0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority=1;
	NVIC_InitStructure.NVIC_IRQChannelCmd=ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_1);
	NVIC_InitStructure.NVIC_IRQChannel=EXTI15_10_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority=0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority=2;
	NVIC_InitStructure.NVIC_IRQChannelCmd=ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	//--------------------------------------------
}

void RX_calculate_value(void)
{
	uint8_t i;
	for(i=0;i<6;i++)
	{
		if(measure_done[i])
		{
			pulse_temp=(stop_ovf_count[i]-start_ovf_count[i])*tm1_max_count+stop_count[i]-start_count[i];
			if((pulse_temp>PULSE_MIN) && (pulse_temp<PULSE_MAX))
			{
				pulse_count[i]=pulse_temp;
			}
			measure_done[i]=0;
		}
	}
	//---- get flying mode ----------
	if(pulse_count[5]<PULSE_MD)
	{
	GPIOB->ODR|=(1<<1);	
	control_mode=1;//gyro mode
	}
	else
	{
	GPIOB->ODR&=~(1<<1);
	control_mode=0;//imu mode
	}
	//-------------------------------
			
	if(control_mode)//gyro mode
	{
	//---- get target x_rate -------
	rx_temp=(float)pulse_count[0]-PULSE_MD_C0;
	if(fabs(rx_temp)>RX_NOISE)
	{
	//rx_value[0]=(MAX_TARGET_RATE_M2*rx_temp)/(PULSE_MAX_C0-PULSE_MIN_C0);
		rx_value[0]=(MAX_TARGET_RATE_M2*rx_temp)/1671;
	}
	else rx_value[0]=0.0;
	//-------------------------------
	
	//---- get target y_rate -------
	rx_temp=(float)pulse_count[1]-PULSE_MD_C1;
	if(fabs(rx_temp)>RX_NOISE)
	{
	//rx_value[1]=(MAX_TARGET_RATE_M2*rx_temp)/(PULSE_MAX_C1-PULSE_MIN_C1);
		rx_value[1]=(MAX_TARGET_RATE_M2*rx_temp)/1845;
	}
	else rx_value[1]=0.0;
	//-------------------------------
	}
	else// angle mode
	{
	//---- get target x_angle -------
	rx_temp=(float)pulse_count[0]-PULSE_MD_C0;
	if(fabs(rx_temp)>RX_NOISE)
	{
	//rx_value[0]=(MAX_TARGET_ANGLE_M2*rx_temp)/(PULSE_MAX_C0-PULSE_MIN_C0);
		rx_value[0]=(MAX_TARGET_ANGLE_M2*rx_temp)/1671;
	}
	else rx_value[0]=0.0;
	//-------------------------------
	
	//---- get target y_angle -------
	rx_temp=(float)pulse_count[1]-PULSE_MD_C1;
	if(fabs(rx_temp)>RX_NOISE)
	{
	//rx_value[1]=(MAX_TARGET_ANGLE_M2*rx_temp)/(PULSE_MAX_C1-PULSE_MIN_C1);
		rx_value[1]=(MAX_TARGET_ANGLE_M2*rx_temp)/1845;
	}
	else rx_value[1]=0.0;
	//-------------------------------
	}
	
	// same as all control modes
	//---- get throttle value -------
	rx_temp=(float)pulse_count[2]-PULSE_MIN_C2;
	rx_value[2]=0.8*rx_temp/2083;//rx_value[2]=rx_temp/(PULSE_MAX_C2-PULSE_MIN_C2);

	//-------------------------------
	
	//---- get target z_rate --------
	rx_temp=(float)pulse_count[3]-PULSE_MD_C3;
	if(rx_temp>3*RX_NOISE)
	{
		rx_value[3]=TARGET_Z_RATE;
	}
	else if(rx_temp<-(3*RX_NOISE))
	{
		rx_value[3]=-TARGET_Z_RATE;
	}
	else
	{
		rx_value[3]=0.0;
	}
	//-------------------------------
}
void EXTI4_IRQHandler(void)
{
	EXTI_ClearITPendingBit(EXTI_Line4);
	if(((GPIOA->IDR)&(1<<4))&&(!measure_done[0]))//rising edge
	{
		start_count[0]=TIM1->CNT;
		start_ovf_count[0]=tm1_ovf_count;
	}
	else 
	{
		stop_count[0]=TIM1->CNT;
		stop_ovf_count[0]=tm1_ovf_count;
		measure_done[0]=1;
		//pulse_count[0]=(stop_ovf_count[0]-start_ovf_count[0])*tm1_max_count+stop_count[0]-start_count[0];
	}
}


void EXTI9_5_IRQHandler(void)
{	
	if(EXTI_GetITStatus(EXTI_Line5))
	{
		EXTI_ClearITPendingBit(EXTI_Line5);
		if(((GPIOA->IDR)&(1<<5))&&(!measure_done[1]))//rising edge
		{
		start_count[1]=TIM1->CNT;
		start_ovf_count[1]=tm1_ovf_count;
		}
		else 
		{
		stop_count[1]=TIM1->CNT;
		stop_ovf_count[1]=tm1_ovf_count;
		measure_done[1]=1;
		//pulse_count[1]=(stop_ovf_count[1]-start_ovf_count[1])*tm1_max_count+stop_count[1]-start_count[1];
		}
	}
	
	else if(EXTI_GetITStatus(EXTI_Line6))
	{
		EXTI_ClearITPendingBit(EXTI_Line6);
		if(((GPIOA->IDR)&(1<<6))&&(!measure_done[2]))//rising edge
		{
		start_count[2]=TIM1->CNT;
		start_ovf_count[2]=tm1_ovf_count;
		}
		else//falling edge
		{
		stop_count[2]=TIM1->CNT;
		stop_ovf_count[2]=tm1_ovf_count;
		measure_done[2]=1;
		//pulse_count[2]=(stop_ovf_count[2]-start_ovf_count[2])*tm1_max_count+stop_count[2]-start_count[2];
		}
	}
	
	else if(EXTI_GetITStatus(EXTI_Line7))
	{
		EXTI_ClearITPendingBit(EXTI_Line7);
		if(((GPIOA->IDR)&(1<<7))&&(!measure_done[3]))//rising edge
		{
		start_count[3]=TIM1->CNT;
		start_ovf_count[3]=tm1_ovf_count;
		}
		else//falling edge
		{
		stop_count[3]=TIM1->CNT;
		stop_ovf_count[3]=tm1_ovf_count;
		measure_done[3]=1;
		//pulse_count[3]=(stop_ovf_count[3]-start_ovf_count[3])*tm1_max_count+stop_count[3]-start_count[3];
		}
	}
	
	else if(EXTI_GetITStatus(EXTI_Line8))
	{
		EXTI_ClearITPendingBit(EXTI_Line8);
		if(((GPIOA->IDR)&(1<<8))&&(!measure_done[4]))//rising edge
		{
		start_count[4]=TIM1->CNT;
		start_ovf_count[4]=tm1_ovf_count;
		}
		else//falling edge
		{
		stop_count[4]=TIM1->CNT;
		stop_ovf_count[4]=tm1_ovf_count;
		measure_done[4]=1;
		//pulse_count[4]=(stop_ovf_count[4]-start_ovf_count[4])*tm1_max_count+stop_count[4]-start_count[4];
		}
	}
}

void EXTI15_10_IRQHandler(void)
{
	if(EXTI_GetITStatus(EXTI_Line11))
	{
		EXTI_ClearITPendingBit(EXTI_Line11);
		if(((GPIOA->IDR)&(1<<11))&&(!measure_done[5]))//rising edge at pin PA11
		{			
		start_count[5]=TIM1->CNT;
		start_ovf_count[5]=tm1_ovf_count;
		}
		else
		{
		stop_count[5]=TIM1->CNT;
		stop_ovf_count[5]=tm1_ovf_count;
		measure_done[5]=1;
		//pulse_count[5]=(stop_ovf_count[5]-start_ovf_count[5])*tm1_max_count+stop_count[5]-start_count[5];
		}
	}
}

