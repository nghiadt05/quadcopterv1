#include "led_and_buttons.h"

uint8_t rf_command;

void LEDS_Configuration(void)
{
	GPIO_InitTypeDef GPIO_Config;
	//---- enable clock for porta, portb and AFIO
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO|
												 RCC_APB2Periph_GPIOB,ENABLE);
	//---- disabled JTAG
	GPIO_PinRemapConfig(GPIO_Remap_SWJ_Disable,ENABLE);
	
	//---- configuration PB[0->3] as LED[0->3] out put
	GPIO_Config.GPIO_Pin= GPIO_Pin_0|GPIO_Pin_1|GPIO_Pin_2|GPIO_Pin_3;
	GPIO_Config.GPIO_Speed=GPIO_Speed_50MHz;
	GPIO_Config.GPIO_Mode=GPIO_Mode_Out_PP;
	GPIO_Init(GPIOB,&GPIO_Config);
	
	GPIOB->ODR&=0xf0;//all leds off
}

void RF_Configuration(void)
{
	//---for button typedefine  ----
	GPIO_InitTypeDef GPIO_Config;
	//------------------------------
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA,ENABLE);
	
	//---- configuration PA4/5/6/7 as in put (button)
	GPIO_Config.GPIO_Pin= GPIO_Pin_4|GPIO_Pin_5|GPIO_Pin_6|GPIO_Pin_7;//pina from 4 to 7
	GPIO_Config.GPIO_Speed=GPIO_Speed_50MHz;
	GPIO_Config.GPIO_Mode=GPIO_Mode_IPD;
	GPIO_Init(GPIOA,&GPIO_Config);
}

uint8_t RF_read_conmmand(void)
{
	rf_command=(((uint16_t)GPIOA->IDR)>>4)&0x0f;
	GPIOB->ODR&=0xf0;//all leds off
	GPIOB->ODR|=rf_command;
	return rf_command;
}

void led(uint8_t led_nb,uint8_t led_stt)
{
		if(led_stt)
		{
		GPIO_WriteBit(GPIOB,1<<led_nb,Bit_SET);
		}
		else
		{
		GPIO_WriteBit(GPIOB,1<<led_nb,Bit_RESET);
		}
}



