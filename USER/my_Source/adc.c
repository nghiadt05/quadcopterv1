#include "adc.h"
#include "pwm_tm2.h"
#include <stdio.h>


uint32_t adc_value;

void ADC_Configuration(void)
{
	ADC_InitTypeDef ADC_Config;
	GPIO_InitTypeDef GPIO_ADC_Config;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1|RCC_APB2Periph_AFIO|
	                       RCC_APB2Periph_GPIOA,ENABLE);
	
	//--  config pina4 as adc input (adc12_channe4) --
	GPIO_ADC_Config.GPIO_Mode=GPIO_Mode_AIN;
	GPIO_ADC_Config.GPIO_Pin=GPIO_Pin_4|GPIO_Pin_5|GPIO_Pin_6|GPIO_Pin_7;
	GPIO_Init(GPIOA,&GPIO_ADC_Config);
	//------------------------------------------------
	
	//----------------  config adc -------------------
	ADC_Config.ADC_ContinuousConvMode=DISABLE;
	ADC_Config.ADC_DataAlign=ADC_DataAlign_Right;
	ADC_Config.ADC_ExternalTrigConv=ADC_ExternalTrigConv_None;
	ADC_Config.ADC_Mode=ADC_Mode_Independent;
	ADC_Config.ADC_NbrOfChannel=4;//not necessary
	ADC_Config.ADC_ScanConvMode=DISABLE;
	ADC_Init(ADC1,&ADC_Config);
	
	//- enable intr when a conversion complete -----
	ADC_ITConfig(ADC1,ADC_IT_EOC, ENABLE);
	ADC_ClearITPendingBit(ADC1,ADC_IT_EOC);
	//----------------------------------------------
	
	// enable adc1
	ADC_Cmd(ADC1, ENABLE); 	
	//------------------------------------------------
}

void ADC_NVIC_Configuration(void)
{
	NVIC_InitTypeDef NVIC_ADC_Config;	
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_0);
	NVIC_ADC_Config.NVIC_IRQChannel=ADC1_2_IRQn;
	NVIC_ADC_Config.NVIC_IRQChannelCmd=ENABLE;
	NVIC_ADC_Config.NVIC_IRQChannelPreemptionPriority=0;
	NVIC_ADC_Config.NVIC_IRQChannelSubPriority=5;
	NVIC_Init(&NVIC_ADC_Config);
}

uint16_t adc_get_value(uint8_t channel)
{
	static uint8_t adc_stt,i;
	adc_value=0;
	for(i=0;i<50;i++)
	{
	ADC_RegularChannelConfig(ADC1,channel,1,ADC_SampleTime_55Cycles5); 
	ADC_SoftwareStartConvCmd(ADC1,ENABLE);	
	while(!adc_stt)//wait until conversion complete
	{
	adc_stt=ADC_GetFlagStatus(ADC1,ADC_FLAG_EOC);
	}
	adc_value+=ADC_GetConversionValue(ADC1);
	}
	adc_value/=200;
	return adc_value;
}

/*
	Disable RF simple then use this function 
*/
void Get_motor_min_value(void)
{
		adc_get_value(0x04);	
		if(adc_value<100)
		{
		TIM2->CCR1=STOP_PWM;
		TIM2->CCR2=STOP_PWM;
		TIM2->CCR3=STOP_PWM;
		TIM2->CCR4=STOP_PWM;
		}
		else
		{
		TIM2->CCR1=adc_value+STOP_PWM;
		TIM2->CCR2=adc_value+STOP_PWM;
		TIM2->CCR3=adc_value+STOP_PWM;
		TIM2->CCR4=adc_value+STOP_PWM;
		}
		//printf("adc_value= %d \r",adc_value+2400);

}
/*
void ADC1_2_IRQHandler(void)
{
	if(ADC_GetITStatus(ADC1,ADC_IT_EOC))
	{
		//clear pending bit for the next conversion detection
		ADC_ClearITPendingBit(ADC1,ADC_IT_EOC);
    //---- store the recent value -------------
		adc_value[i]=ADC_GetConversionValue(ADC1);
		//---- change to the next channel ---------
		i++;
		i%=4;
		ADC_RegularChannelConfig(ADC1,0x04+i,1,ADC_SampleTime_55Cycles5); 
    ADC_SoftwareStartConvCmd(ADC1,ENABLE);	
	}
}
*/

/*
//----------------- read adc value in 4 channels --------------------
for(i=0;i<4;i++)
{
ADC_RegularChannelConfig(ADC1,0x04+i,1,ADC_SampleTime_55Cycles5); 
ADC_SoftwareStartConvCmd(ADC1,ENABLE);			
adc_stt=ADC_GetFlagStatus(ADC1,ADC_FLAG_EOC);
while(!adc_stt)//wait until conversion complete
{
adc_stt=ADC_GetFlagStatus(ADC1,ADC_FLAG_EOC);
}
adc_value[i]=ADC_GetConversionValue(ADC1);
}
//--------------------------------------------------------------------
*/			




