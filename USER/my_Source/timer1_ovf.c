#include "stm32f10x.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_tim.h"
#include "misc.h"
#include <stdio.h>
#include "timer1_ovf.h"

//---------for calculating loop time-----------
uint32_t tm1_ovf_count;
uint32_t last_count,last_ovf_time;
uint32_t present_count,present_ovf_time;
uint32_t loop_time_count;
float loop_time;
//------------+++++++++++----------------------

void TM1_OVF_Configuration(void)
{
	TIM_TimeBaseInitTypeDef TM1_OVF_Config;
	NVIC_InitTypeDef NVIC_TM1_Config;
	
	//----- setup timer1 for overflows intr -------
	// t_step=(36)/(72Mhz)=0.5us
	// T= t_step*Period= 0.025ms 
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1,ENABLE);
	TM1_OVF_Config.TIM_ClockDivision=TIM_CKD_DIV1;
	TM1_OVF_Config.TIM_CounterMode=TIM_CounterMode_Up;
	TM1_OVF_Config.TIM_Period=tm1_max_count;
	TM1_OVF_Config.TIM_Prescaler=36-1;
	TM1_OVF_Config.TIM_RepetitionCounter=0;
	TIM_TimeBaseInit(TIM1,&TM1_OVF_Config);
	
	TIM_ITConfig(TIM1,TIM_IT_Update,ENABLE);
	TIM_ClearITPendingBit(TIM1,TIM_IT_Update);
	TIM_Cmd(TIM1,ENABLE);
	//---------------+++++++++---------------------
	
	//---- setup nivic for timer1 ovf interrupt -----
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_0);
	NVIC_TM1_Config.NVIC_IRQChannel=TIM1_UP_IRQn;
	NVIC_TM1_Config.NVIC_IRQChannelCmd=ENABLE;
	NVIC_TM1_Config.NVIC_IRQChannelPreemptionPriority=0;
	NVIC_TM1_Config.NVIC_IRQChannelSubPriority=1;
	NVIC_Init(&NVIC_TM1_Config);
	//----------------+++++++------------------------
}

float get_loop_time(void)
{
	present_count=TIM1->CNT;
	present_ovf_time=tm1_ovf_count;
	loop_time_count=(present_ovf_time-last_ovf_time)*tm1_max_count+present_count-last_count;
	last_count=present_count;
	last_ovf_time=present_ovf_time;
	loop_time=(float)loop_time_count*5e-7;
	//printf("Test 1: %f \r",loop_time*1000);
	return loop_time;
}


void TIM1_UP_IRQHandler(void)
{
	TIM_ClearITPendingBit(TIM1,TIM_IT_Update);
	//   tm1_ovf_count_max=2e33-1 
	//=> max_time=tm1_ovf_count_max*T= (2e33-1)0.25ms=2.48 days
	// So we never mind for our robot system
	tm1_ovf_count++;
}




