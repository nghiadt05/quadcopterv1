#include "usart3.h"

uint16_t rx3_temp;
void USART3_Configuration(void)
{ 
  GPIO_InitTypeDef GPIO_InitStructure;// define object
  USART_InitTypeDef USART_InitStructure; 

  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB|RCC_APB2Periph_AFIO,ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3,ENABLE);
				
	//--- config tx3 pin PB10
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;	         
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;// can't change to GPIO_Mode_Out_PP
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; 
  GPIO_Init(GPIOB, &GPIO_InitStructure);	
	//config rx3 pin PB11
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11;	        
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;  
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; 
  GPIO_Init(GPIOB, &GPIO_InitStructure);

  USART_InitStructure.USART_BaudRate = 115200;
  USART_InitStructure.USART_WordLength = USART_WordLength_8b;
  USART_InitStructure.USART_StopBits = USART_StopBits_1;
  USART_InitStructure.USART_Parity = USART_Parity_No;
  USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
  USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
  USART_Init(USART3, &USART_InitStructure);//apply change for usart3 
	
  USART_ITConfig(USART3, USART_IT_RXNE, ENABLE);//enable intr when recived complete
  //USART_ITConfig(USART3, USART_IT_TXE, ENABLE);//enable intr when a transmittion complete
  USART_ClearFlag(USART3,USART_FLAG_TC);//clear this flag to prepare a new transmittion
  USART_Cmd(USART3, ENABLE);//enable usart1
}

//------ nvic setupt for usart1 intr ----------
void USART3_NvicSetup(void)//enable global USAR1_intr
{
	NVIC_InitTypeDef NVIC_InitStructure; 
  NVIC_PriorityGroupConfig(NVIC_PriorityGroup_0);  												
  NVIC_InitStructure.NVIC_IRQChannel = USART3_IRQn ;// 
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;   
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;	      
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);//apply setting for usart3_global_intr
}

void USART3_put_char(uint8_t ch)
{
	USART_SendData(USART3,ch);
	while (USART_GetFlagStatus(USART3, USART_FLAG_TC) == RESET){}
}
