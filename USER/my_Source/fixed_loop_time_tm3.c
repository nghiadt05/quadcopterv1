#include "stm32f10x.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_tim.h"
#include "misc.h"
#include "systick.h"
#include "fixed_loop_time_tm3.h"

void TM3_FLT_Configuration(void)
{
	TIM_TimeBaseInitTypeDef TIM_Config;	
	NVIC_InitTypeDef NVIC_TM3_Config;
	
	//------------ setup for tm3 ------------------------
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3,ENABLE);
	//f_step=(72MHz/1)/(Prescaler+1)=2Mhz
	//t_step=1/f_step=0.5us
	//T= fixed_loop_time = t_step*Period=0.5 us*2000=1ms
	TIM_Config.TIM_ClockDivision=TIM_CKD_DIV1;
	TIM_Config.TIM_CounterMode=TIM_CounterMode_Up;
	TIM_Config.TIM_Period=2000;//1ms per overflowtime
	TIM_Config.TIM_Prescaler=(36-1);
	TIM_TimeBaseInit(TIM3,&TIM_Config);
	
	TIM_ITConfig(TIM3,TIM_IT_Update,ENABLE);//enable timer3 ovf intr
	TIM_ClearITPendingBit(TIM3,TIM_IT_Update);//for first detection
	TIM_Cmd(TIM3, ENABLE);//enable timer2		
	//----------------++++++++++---------------------
	
	//--------- nvic setup for timer2 ----------
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_0);
	NVIC_TM3_Config.NVIC_IRQChannel=TIM3_IRQn;
	NVIC_TM3_Config.NVIC_IRQChannelCmd=ENABLE;
	NVIC_TM3_Config.NVIC_IRQChannelPreemptionPriority=0;
	NVIC_TM3_Config.NVIC_IRQChannelSubPriority=3;
	NVIC_Init(&NVIC_TM3_Config);
	//---------------+++++++++++----------------
}

void TIM3_IRQHandler(void)
{
	if(TIM_GetITStatus(TIM3,TIM_IT_Update))
	{
		TIM_ClearITPendingBit(TIM3,TIM_IT_Update);//for next detection
		temp_count++;
		sys_time_count=temp_count;
	}
}

void fixed_loop_time(void)
{
	loop_usefull_count=sys_time_count-last_loop_count;
	while(loop_usefull_count<fixed_loop_count)
	{
		loop_usefull_count=sys_time_count-last_loop_count;
		delay_us(1);
	}
	last_loop_count=sys_time_count;
}
