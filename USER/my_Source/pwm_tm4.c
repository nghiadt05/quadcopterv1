#include "pwm_tm4.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_tim.h"

uint16_t servo_value[2];
void TM4_PWM_Configuration(void)
{
	// Setup PWM by using timer4
	// T=20ms
	// t_on=0.8-2.2 ms (800-2200)
	// 2 pwm chanels: pb8 (tm4_ch1), pb9 (tm4_ch2)
	TIM_TimeBaseInitTypeDef TM4_TB_Config;
	TIM_OCInitTypeDef TM4_OC_Config;
	GPIO_InitTypeDef GPIO_PWM_CHN_Config;
	
	//--- enable clock source for timer4, gpiob and afio------
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB|RCC_APB2Periph_AFIO,ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4,ENABLE);
	//------------------------++++++++++++++++++---------------------
	
	//--- setup pb[8-9] as pwm outputs ------------
	GPIO_PWM_CHN_Config.GPIO_Mode=GPIO_Mode_AF_PP;
	GPIO_PWM_CHN_Config.GPIO_Pin=GPIO_Pin_8|GPIO_Pin_9;
	GPIO_PWM_CHN_Config.GPIO_Speed=GPIO_Speed_50MHz;
	GPIO_Init(GPIOB,&GPIO_PWM_CHN_Config);
	//---------------+++++++++++-------------------
	
	//------------ setup timer4 -------------------
	//first, setup timebase structure
	TM4_TB_Config.TIM_ClockDivision=TIM_CKD_DIV1;
	TM4_TB_Config.TIM_CounterMode=TIM_CounterMode_Up;
	TM4_TB_Config.TIM_Period=20000;//T=Period*t_step=20ms
	TM4_TB_Config.TIM_Prescaler=72-1;//t_step=((Prescaler+1)/2)/timer_clock=(36)/(36Mhz)=1us
	TIM_TimeBaseInit(TIM4,&TM4_TB_Config);
	
	//second, setup output compare structure
	TM4_OC_Config.TIM_OCMode=TIM_OCMode_PWM1;
	TM4_OC_Config.TIM_OCPolarity=TIM_OCPolarity_High;
	TM4_OC_Config.TIM_OutputState=TIM_OutputState_Enable ;
	TM4_OC_Config.TIM_Pulse=0;
	TIM_OC3Init(TIM4,&TM4_OC_Config);
	TIM_OC4Init(TIM4,&TM4_OC_Config);
	
	//enable auto reload for OC_reg and ARR_reg
	TIM_ARRPreloadConfig(TIM4,ENABLE);//can change T 
	TIM_OC3PreloadConfig(TIM4,ENABLE);//can change t_on
	TIM_OC4PreloadConfig(TIM4,ENABLE);//can change t_on
	TIM_Cmd(TIM4,ENABLE);
	//---------------+++++++-----------------------
	
	Servo_init();
}


void Servo_init(void)
{
	TIM4->CCR3=SG90_MIDDLE;
	TIM4->CCR4=HK939_MIDDLE;
}

void Servo_out(void)
{
	servo_value[0]=(uint16_t) (SG90_MIDDLE+(rpy[1]*SG90_SCALE/10));
	if(servo_value[0]>SG90_MAX)servo_value[0]=SG90_MAX;
	if(servo_value[0]<SG90_MIN)servo_value[0]=SG90_MIN;
	
	servo_value[1]=(uint16_t) (HK939_MIDDLE-(rpy[0]*HK939_SCALE/10));
	if(servo_value[1]>HK939_MAX)servo_value[1]=HK939_MAX;
	if(servo_value[1]<HK939_MIN)servo_value[1]=HK939_MIN;
	
	TIM4->CCR3=servo_value[0];
	TIM4->CCR4=servo_value[1];
}
/*
void esc_calib(void)
{
	TIM2->CCR1=MAX_PWM;
	TIM2->CCR2=MAX_PWM;
	TIM2->CCR3=MAX_PWM;
	TIM2->CCR4=MAX_PWM;
	delay_ms(2000);
	TIM2->CCR1=MIN_PWM;
	TIM2->CCR2=MIN_PWM;
	TIM2->CCR3=MIN_PWM;
	TIM2->CCR4=MIN_PWM;
	delay_ms(2000);
}
*/

