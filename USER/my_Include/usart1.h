#include "stm32f10x.h"
#include "led_and_buttons.h"
#include <stdio.h>

#define ready2send 255

extern uint16_t rx1_temp;
extern uint8_t port;

void USART1_Configuration(void);
void USART1_NvicSetup(void);
void USART1_IRQHandler(void);
