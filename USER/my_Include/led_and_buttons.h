#ifndef _LED_BT_
#define _LED_BT_

#include "stm32f10x.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_gpio.h"
#define on 1
#define off 0

extern uint8_t rf_command;

void LEDS_Configuration(void);
void RF_Configuration(void);
void led(uint8_t led_nb,uint8_t led_stt);
uint8_t RF_read_conmmand(void);

#endif
