#ifndef _USART3_H_
#define _USART3_H_

#include "stm32f10x.h"
#include "stm32f10x_usart.h"

extern uint16_t rx3_temp;
void USART3_Configuration(void);
void USART3_NvicSetup(void);
void USART3_IRQHandler(void);
void USART3_put_char(uint8_t ch);

#endif
