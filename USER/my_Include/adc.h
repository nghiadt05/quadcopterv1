#ifndef _ADC_H_
#define _ADC_H_

#include "stm32f10x.h"
#include "stm32f10x_adc.h"

extern uint32_t adc_value;

void ADC_Configuration(void);
void ADC_NVIC_Configuration(void);
uint16_t adc_get_value(uint8_t channel);
void Get_motor_min_value(void);

#endif
