//=====================================================================================================
// MadgwickAHRS.h
//=====================================================================================================
//
// Implementation of Madgwick's IMU and AHRS algorithms.
// See: http://www.x-io.co.uk/node/8#open_source_ahrs_and_imu_algorithms
//
// Date			Author          Notes
// 29/09/2011	SOH Madgwick    Initial release
// 02/10/2011	SOH Madgwick	Optimised for reduced CPU load
// 19/02/2012	SOH Madgwick	Magnetometer measurement is normalised
//
//=====================================================================================================

//---------------------------------------------------------------------------------------------------
#ifndef _S_AHRS_H_
#define _S_AHRS_H_
#include "stm32f10x.h"

#ifndef M_PI
#define M_PI 3.14159265358979323846	/* pi */
#endif

//---------------------------------------------------------------------------------------------------
// Variable definitions
extern float beta ;								// 2 * proportional gain (Kp)
extern float q0,q1,q2,q3;	// quaternion of sensor frame relative to auxiliary frame
extern float rpy[3],Eangle[3];
//---------------------------------------------------------------------------------------------------
// Function declarations
float invSqrt(float x);
void MadgwickAHRSupdateIMU(float gx, float gy, float gz, float ax, float ay, float az, float dt);
void MadgwickAHRSupdate(float gx, float gy, float gz, float ax, float ay, float az, float mx, float my, float mz, float dt);
void getYawPitchRoll(void);
void getEangle(void);
//====================================================================================================
#endif
