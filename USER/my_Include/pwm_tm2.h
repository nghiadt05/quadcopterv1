#ifndef _PWM_TM2_H_
#define _PWM_TM2_H_

#define MAX_PWM 3800
#define MIN_PWM 2000
#define PWM_RANGE 1800
#define STOP_PWM 1600

void TM2_PWM_Configuration(void);
void Motor_init(void);
void Motor_Disabled(void);

#endif
