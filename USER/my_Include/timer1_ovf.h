#ifndef _TIMER1_OVF_H
#define _TIMER1_OVF_H
#include "stm32f10x.h"

//---------for calculating loop time-----------
#define tm1_max_count 50000
 extern uint32_t tm1_ovf_count;
 extern uint32_t last_count,last_ovf_time;
 extern uint32_t present_count,present_ovf_time;
 extern uint32_t loop_time_count;
 extern float loop_time;
//------------+++++++++++----------------------

void TM1_OVF_Configuration(void);
float get_loop_time(void);
void TIM1_UP_IRQHandler(void);


/*
//---------for measuring pulse width of 6 rx chanels------ 
 extern uint32_t start_ovf_count[6];//6 input pwm channels
 extern uint32_t stop_ovf_count[6];
 extern uint32_t start_count[6];
 extern uint32_t stop_count[6];
 extern uint32_t pulse_count[6];
 extern float pulse_time[6];//store the pulse times of 6 input pwm chanels from rx device
//-----------++++++++++++++++++++++++++++------------------
*/
/*
void EXTI4_IRQHandler(void);
void EXTI9_5_IRQHandler(void);
*/
#endif
