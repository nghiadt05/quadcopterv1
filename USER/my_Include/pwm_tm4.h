#ifndef _PWM_TM4_H_
#define _PWM_TM4_H_

#include "stm32f10x.h"
#include "ahrs.h"

extern uint16_t servo_value[2];

#define SG90_MIN 560
#define SG90_MIDDLE 1384
#define SG90_MAX 2299
#define SG90_SCALE 96// count/10 degree

#define HK939_MIN 635
#define HK939_MIDDLE 1498
#define HK939_MAX 2425
#define HK939_SCALE 99// count/10 degree

void TM4_PWM_Configuration(void);
void Servo_init(void);
void Servo_out(void);
#endif
