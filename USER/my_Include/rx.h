#ifndef _RX_H_
#define _RX_H_

#include "stm32f10x.h"

#define MAX_TARGET_ANGLE_M2 60.0//20.0*2
#define MAX_TARGET_ANGLE		30.0//20.0*2
#define MAX_TARGET_RATE_M2 200.0//100.0*2
#define MAX_TARGET_RATE 	 100.0
#define TARGET_Z_RATE 150

#define RX_NOISE 50

#define PULSE_MIN_C0 2111
#define PULSE_MD_C0 2975
#define PULSE_MAX_C0 3782

#define PULSE_MIN_C1 2063
#define PULSE_MD_C1 2981
#define PULSE_MAX_C1 3908

#define PULSE_MIN_C2 1780
#define PULSE_MAX_C2 4183

#define PULSE_MIN_C3 2126
#define PULSE_MD_C3 3002
#define PULSE_MAX_C3 3815

#define PULSE_MD 3000//1.5ms
#define PULSE_LOCK_MAX 3600
#define PULSE_LOCK_MIN 2200
#define PULSE_MAX 4500
#define PULSE_MIN 1600


//---------for measuring pulse width of 6 rx chanels------ 
extern uint32_t start_ovf_count[6];//6 input pwm channels
extern uint32_t stop_ovf_count[6];
extern uint32_t start_count[6];
extern uint32_t stop_count[6];
extern uint32_t pulse_count[6],pulse_temp;
extern uint8_t control_mode;//=1: gyro mode; =0: auto balance mode
extern float rx_temp;
extern float rx_value[6];//store the pulse times of 6 input pwm chanels from rx device
											 	// rx_value[0]: target_x_angle/target_x_rate
												// rx_value[1]: target_y_angle/target_y_rate
												// rx_value[2]: throttle
												// rx_value[3]: target_z_angle/target_z_rate
												// rx_value[4]: (unused)
												// rx_value[5]: (unused)
extern uint8_t measure_done[6];
//-----------++++++++++++++++++++++++++++------------------

/*
			Functions
*/
void RX_Configuration(void);
void RX_calculate_value(void);
void EXTI4_IRQHandler(void);
void EXTI9_5_IRQHandler(void);
void EXTI15_10_IRQHandler(void);

#endif	
