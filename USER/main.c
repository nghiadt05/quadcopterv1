/*
	Author: Doan Trung Nghia D09DT2 
	Email: doantrungnghia05@gmail.com
*/
#include "stm32f10x.h"
#include "usart1.h"
#include "usart3.h"
#include "rcc_config.h"
#include "systick.h"
#include "fixed_loop_time_tm3.h"
#include "pwm_tm2.h"
#include "pwm_tm4.h"
#include "timer1_ovf.h"
#include "pid.h"
#include "led_and_buttons.h"
#include "ahrs.h"
#include "i2c_sensors.h"
#include "rx.h"
uint8_t i,start=0;
int main(void)
{
		//----------- system init ---------
		RCC_use_ext_crystal();
		DELAY_Configuration();
		TM1_OVF_Configuration();
		TM2_PWM_Configuration();
	  TM3_FLT_Configuration();	
	  TM4_PWM_Configuration();
		LEDS_Configuration();
	  USART1_Configuration();
	  USART1_NvicSetup();
		USART3_Configuration();
	  USART3_NvicSetup();
		I2C_Configuration();
		RX_Configuration();
		//printf("Internal init done\r");
		//----------------------------------
	
		//--init  external components ------
		mpu_6050_init();
		//hmc5883_init();		
		Set_pid_gain_value();
		Motor_Disabled();
		//printf("External init done\r");
		//----------------------------------		
    while (1)
		{
		//---- get loop time for per loop---------	
		get_loop_time();
		//----------------------------------------	
		
		//------read command from RF controller---
		RX_calculate_value();	
		if(!start)
		{
			if((pulse_count[0]>PULSE_LOCK_MAX) && (pulse_count[1]<PULSE_LOCK_MIN))
			{
			start=1;
			GPIOB->ODR|=(1<<3);	
			}
		}
		//----------------------------------------
			
		//-- get data from mpu6050 and hmc5883l--
		mpu6050_get_value();
		get_gyro_rate();
		get_acc_value();
		//----------------------------------------	
		
		//--------- ahrs computing ---------------					
		MadgwickAHRSupdateIMU(gyro_x_rate*M_PI/180,gyro_y_rate*M_PI/180,gyro_z_rate*M_PI/180,
											acc_x_temp,acc_y_temp,acc_z_temp);
		//getEangle();
		getRollPitchYaw();
		//----------------------------------------	
					
		//--------- pid controller and output to motors -----	
		if(start)
		{
		 Get_pid_gain_value();//get pid gain value from Processing if requested
		 Send_pid_gain();//send if requested
		 PID_x_update();
		 PID_y_update();
		 PID_z_update();
		 MORTOR_output();
		 //Servo_out();
		}
		//printf("%f\r",rpy[1]);
		//printf("%d  %d  %f\r",(int)rpy[0],(int)rpy[1],loop_time*1000);
		//printf("%d %d\r",servo_value[0],servo_value[1]);
		//printf("%d %d %d\r",(int)rpy[1],(int)PID_y,(int)(rx_value[2]*100));
		//printf("%d %d %d\r",(int)rpy[0],(int)PID_x,(int)(rx_value[2]*100));
		//printf("%d %d %d %0.2f\r",(int)Pg_temp,(int)Ig_temp,(int)Dg_temp,rx_value[2]);
		//printf("%f\r",loop_time*1000);
		//printf("%d %0.1f\r",(int)pid_z_out_temp,rx_value[3]);
		//printf("%d %d\r",pulse_count[2],(int)(rx_value[2]*1000));
		//printf("%d %d %d %d\r",(int)rpy[0],(int)rpy[1],(int)rpy[2],(int)(loop_time*10000));
		//---------------------------------------------------	
		
		//---------------------------------------------------
		fixed_loop_time();
    }
}

//debug
		//printf("%d  %d  %d %f\r",(int)Eangle[0],(int)Eangle[1],(int)Eangle[2],loop_time*1000);
		/*
		for(i=0;i<4;i++)
		{
			printf("%0.2f ",rx_value[i]);
		}
		printf("\r");
		*/
		/*
		if(rx3_temp>0)
		{
		port=3;
		i2c_sensors_send_prcessing_data(Eangle[0],Eangle[1],Eangle[2]);
		}	
		*/
		/*
		printf("%d ",page);
		for(i=0;i<3;i++)
		{
			printf("%0.3f ",pid_gain_vl[3*page+i]);
		}
		printf("\r");
		*/
		/*
		for(i=0;i<6;i++)
		{
			printf("%d ",pulse_count[i]);
		}
		printf("\r");
		*/
