# Abstract#

This is the first version of my quadcopter project, which is the graduation project of my undergraduate course in an Electrical and Electronic Engineering program.

Here is some information of the project:

- Using self-design PCB board which consists of an 9DOF IMU and an Arm-Cortex M3 micro controller (Designed by my friend - Nhuong Trieu).
- Simple calibration technique for the IMU block
- Using a Kalman filter to calculate the Euler angles of the quadcopter
- Integrating a humidity, a temperature and a barometer sensors to collect additional information of the surrounding environment. Those data is then sent back to a computer placed on the ground to display all status of the quadcopter
- PID tunning using the remote controller (Joysticks) 
- An user interface program is accompanied with the program package 

The coding environment is KeilC for ARM and the user interface is written using Processing.

The performance of this quadcopter is demonstrated here: [Demo link](https://www.youtube.com/watch?v=z6fBWdlH8xI)

My quadcopter V1 appearance:

![Capture.PNG](https://bitbucket.org/repo/edEyqx/images/1615254682-Capture.PNG)

The user interface program is posted here: [Demo link](https://www.youtube.com/watch?v=lhocndwFazY)

The user interface (using an RF module to transmit data):

![Capture.PNG](https://bitbucket.org/repo/edEyqx/images/3524989322-Capture.PNG)