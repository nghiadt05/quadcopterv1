import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import processing.serial.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class my_quad_graphic extends PApplet {


Serial port;
PFont font,font2;
PImage img,img1,img2;
float x,y,z;
float temp,press=1000,alt;
int sub_x,sub_y,sub_z,sub_temp,sub_press,sub_alt;
//------------- serial port value ---------------
int ser_port_nb, Start;
int new_x, new_y, box_loc, chose_port=0, mouse_click;
int n_x,n_y,click_event,click_check,mode=0;
//-----------------------------------------------

//----------- about project ------------------
String txt[]=new String[30];        
//---------------------------------------------

public void setup() {
  size(800, 680, P3D); 
  frameRate(30);
  smooth();
  noStroke();
  //---------- load font -------------
  font= loadFont("Andalus-25.vlw");
  font2= loadFont("DilleniaUPCBold-48.vlw");
  textFont(font2);
  textSize(20);
  //----------------------------------
  //------- setup color mode ---------
  colorMode(RGB, 1); 
  //----------------------------------
  //------- load image ---------------
  img=loadImage("quad2.PNG");
  img1=loadImage("me.png");
  img2=loadImage("ptit.png");
  image(img,0,0);
  //-------- text setup --------------
  setup_text();  
}

public void draw() { 
 
  clear();
  image(img,0,0);
  //------------------------- change mode -------------------------------
  if(mouseX>705 && mouseX<785 && mouseY>130 && mouseY<160)//change mode
  {
    mode=1;
  }
  else mode=0;
  //text(mouseX + " " + mouseY,200,300);
  //---------------------------------------------------------------------
  if(mode==0)
  {
  rect(705,130,80,30);
  fill(0xffffffff);
  textSize(25);
  text("About prj",710,150);
  serial_control();
  draw_text();
  draw_baro(); 
  draw_compass();
   draw_temp();
  draw_roll_axis();
  draw_pitch_axis();
  draw_3d_motion(); 
  }
  else if(mode==1)
  {
   fill(0xffff0000);
   textSize(40);
   text("ABOUT PROJECT",300,150);
   scale(1.3f);
   image(img1,480,150);
   scale(1/1.3f);
   print_txt();
  }
} 

public void mousePressed()
{
  if(mouseX>20 && mouseX<110 && mouseY>120 && mouseY<280)
  {
  switch (click_event)
  {
    case 1:
      //------ connect to the chosen com port ------
      click_check=1;
      port = new Serial(this,Serial.list()[chose_port-1], 9600);
      port.bufferUntil('\n'); 
      break;
     case 2:
      //------ connect to the chosen com port ------
      click_check=2;
      //--------------------------------------------
      break;
  }
  }
}

public void serialEvent(Serial port) {
  String inputString = port.readStringUntil((int) '\n');
  
  if (inputString != null) {
    String [] inputStringArr = split(inputString, ",");
    
    x = PApplet.parseFloat(inputStringArr[0]);
    y = PApplet.parseFloat(inputStringArr[1]);
    z = PApplet.parseFloat(inputStringArr[2]);
    temp= PApplet.parseFloat(inputStringArr[3]);
    press= PApplet.parseFloat(inputStringArr[4]);
    alt= PApplet.parseFloat(inputStringArr[5]);
  }
} 

public void serial_control()
{
  //----------------------------------------------------------------- serial port controller ----------------------------------------
  ser_port_nb=Serial.list().length;//get theumber of current com port
  //---------------- draw available com windows ---------
  fill(0xff2A2FF5);
  translate(width/30, height/5);
  n_x=width/30;
  n_y=height/5;
  for (int i=0;i<ser_port_nb;i++)
  {
    //----check mouse location ------ 
    if (mouseY>n_y && mouseY<(ser_port_nb*30+n_y))
    {
      if (mouseX>n_x && mouseX<n_x+80)
      {
        box_loc=(mouseY-n_y)/30+1;
      }
    }
    else
    {
      box_loc=0;
    }
    //--------------------------------
   
    if ((i+1)==box_loc)
    {
     if(mousePressed && mouseButton==LEFT)
     {
       click_event+=1;
       if(click_event>2)click_event=1;
       if(click_event==1)//draw green on chosen box
       {
         chose_port=box_loc;
       }
     }
    } 
    
   //------conect/disconect then print notice ------------- 
    textSize(25);
    if(click_check==1)
    {
    fill(0xff2702D1);
    text("OPEN",0,30*i+50);
    if((i+1)!=chose_port)
      {
        fill(0xffD17E02);
      }
    }
    else if(click_check==2)
    {
     fill(0xffD17E02);
     text("CLOSE",0,30*i+50);
    }
    rect(0, 30*i, 80, 30);
    //------------------------------------------------
   

   
    
    //---------------------------------------------------

    fill(0xffffffff);
    textSize(30);
    text(Serial.list()[i], 10, 30*i+25); 
  }

  translate(-width/30, -height/5);
  //---------------------------------------------------
  
  
 //---------------------------------------------------------------------------------------------------------------------------------
}

public void draw_text()
{
  //-------- draw text -------------------------
 translate(width/2,height/6);
 textFont(font2);
 textSize(50);
 fill(0xffED0E0E);
 text("DATA DISPLAY",-100,50);
 textSize(25);
 text("Quadcopter project _v1_ - Doan Trung Nghia - D09DTMT",-190,70);
 translate(-width/2,-height/6);
  
 translate(width/2,height/4); 
 textFont(font2); 
 textSize(30);
 fill(0xff11ED0E);
 sub_x=(int)(x*10);
 sub_y=(int)(y*10);
 sub_z=(int)(z*10);
 sub_temp=(int)(temp*10);
 sub_press=(int)(press*10);
 sub_alt=(int)(alt*10);
 text("Roll [deg]  = "+ sub_x/10 + "." + abs(sub_x%10) ,-250,50);
 text("Pitch [deg] = "+sub_y/10 + "." + abs(sub_y%10) ,-250,75);
 text("Yaw [deg]  = "+sub_z/10 + "." + abs(sub_z%10) ,-250,100);
 text("Temp [oC]  = "+sub_temp/10 + "." + abs(sub_temp%10)  ,50,50);
 text("Press [mBar] = "+sub_press/10 + "." + abs(sub_press%10) ,50,75);
 text("Alt [m] = "+sub_alt/10 + "." + abs(sub_alt%10) ,50,100);
 fill(0xffff0000);
 textSize(45);
 text("3D EMOTION", -80,150);
 translate(-width/2,-height/4); 
 //--------------------------------------------
}

public void draw_baro()
{
   //----------- draw barometter -----------------
  translate(width/7,height*5/7);  
  //-------------- barometer ruler --------
  
   for(char i=0;i<3;i++)
  {
  stroke(0xffFFFFFF);
  strokeWeight(2);
  line(-20,-80*i,-60,-80*i);
  textSize(30);
   fill(0xff0AF713);
  text(i*20+980, -100,-80*i);  
  }
   
   for(char i=0;i<5;i++)
  {
  stroke(0xffFFFFFF);
  strokeWeight(2);
  line(-20,-40*i,-40,-40*i);
  textSize(25);
  if((i%2)!=0)
  {
  fill(0xff0AF713);
  text(i*10+980, -80,-40*i);
  }
  }
  
   for(char i=0;i<9;i++)
  {
  stroke(0xffFFFFFF);
  strokeWeight(2);
  line(-20,-20*i,-30,-20*i);
  }
  if(press>1020)press=1020;
  if(press<980)press=980;
  //---------------------------------------
  
  beginShape(QUADS);
  fill(0xff838683);
  vertex(-20,0);
  vertex(20,0);
  vertex(20,-160);
  vertex(-20,-160);
  
  fill(0xff0778ED);
  vertex(-20,0);
  vertex(20,0);
  vertex( 20,-160*(int)(press-980)/40);
  vertex(-20,-160*(int)(press-980)/40);
  endShape();


  
  fill(0xff0AF713);
  textSize(25);
  text("(mBar)",-60,-170);
  textSize(30);
  text("Barometer",-40,20);
  translate(-width/7,-height*5/7);
 //----------- end of barometer ---------------
}

public void draw_compass()
{
   //--------------- draw compass --------------
  translate(width/7,height*6/7);
  //------------------------------
  noStroke();
  fill(0xff2F22C6);
  ellipse(0,0,120,120);
  fill(0xff90A08E);
  ellipse(0,0,80,80);
  //-----------------------------
  fill(0xffC4C04D);
  textFont(font2);
  textSize(35);
  text("N",-8,-41);
  text("W",-60,8);
  text("E",42,8);
  text("S",-7,58);
  fill(0xff0AF713);
  textSize(30);
  text("E-Compass",-45,80);
 //-------------------------------
 
  rotateZ(radians(z));
  beginShape(QUADS);
  fill(0xffFF0000);
  vertex(-5,0);
  vertex(5,0);
  vertex(5,-25);
  vertex(-5,-25);  
  endShape();
  
  noStroke();
  beginShape();
  vertex(-10,-25);
  vertex(0,-35);
  vertex(10,-25);
  endShape();
  rotateZ(-radians(z));
  translate(-width/7,-height*6/7); 

 //------------------- end of draw compass -----
}

public void draw_roll_axis()
{
  //------------- draw roll degree ruler --------------
 translate(width*6/7,height*4/7);
  
   //--- print number of degree ----
  fill(0xff03FC0C);
  textSize(30);
  text(0,-5,-70);
  
  textSize(30);
  text("Roll axis",-30,90);
  //--------------------------------
  
  //---- print roll degree ruler ---
  stroke(0xffffffff);
  strokeWeight(2);
  for(int i=0;i<361;i+=30)
  {
    rotateZ((float)radians(i));
    line(0,45,0,60);
    rotateZ(-(float)radians(i)); 
  }
  
  for(int i=0;i<361;i+=15)
  {
    rotateZ((float)radians(i));
    line(0,55,0,60);
    rotateZ(-(float)radians(i));
  }
  
  strokeWeight(2);
  stroke(0xffffffff);
  noFill();
  ellipse(0,0,120,120);
  
  fill(0);
  ellipse(0,0,90,90);
  //----------------------------------
  
  //----- draw roll pointer ----------
  rotateZ(radians(x));
  stroke(0xffff0000);
  strokeWeight(4);
  line(-40,0,40,0);
  line(0,0,0,-20);
  rotateZ(-radians(x));
  noStroke();
  //----------------------------------
  translate(-width*6/7,-height*4/7);
 //---------------------------------------------
}

public void draw_pitch_axis()
{
   //-------------- pitch axis -------------------
  translate(width*6/7,height*6/7);
  
   //--- print number of degree ----
  fill(0xff03FC0C);
  textSize(30);
  text(0,70,5);
  text("Pitch axis",-40,80);
  //--------------------------------
  
  //---- print pitch degree ruler ---
  stroke(0xffffffff);
  strokeWeight(2);
  for(int i=0;i<361;i+=30)
  {
    rotateZ((float)radians(i));
    line(0,45,0,60);
    rotateZ(-(float)radians(i)); 
  }
  
  for(int i=0;i<361;i+=15)
  {
    rotateZ((float)radians(i));
    line(0,55,0,60);
    rotateZ(-(float)radians(i));
  }
  
  strokeWeight(2);
  stroke(0xffffffff);
  noFill();
  ellipse(0,0,120,120);
  
  strokeWeight(2);
  stroke(0xffffffff);
  noFill();
  ellipse(0,0,120,120);
  
  fill(0);
  ellipse(0,0,90,90);
  //----------------------------------
  
  //----- draw pitch pointer ----------
  rotateZ(radians(y));
  noStroke();
  fill(0xffff0000);
    beginShape(QUADS);
    vertex(-30,5);
    vertex(-30,-5);
    vertex(15,-5);
    vertex(15,5);
    endShape();
    
    beginShape();
    vertex(15,-5);
    vertex(15,-10);
    vertex(30,0);
    vertex(15,10);
    vertex(15,5);
    endShape();
  rotateZ(-radians(y));
  //----------------------------------
  translate(-width*6/7,-height*6/7);
 //---------------------------------------------
}

public void draw_3d_motion()
{
  //-------------- draw3d motion -------------------------------------- 
  translate(width/2, height*2/3+30); 
 
  rotateX(radians(y));
  rotateY(-radians(z));
  rotateZ(radians(x));
  scale(3.2f);
  beginShape(QUADS);
  // z+ (to the drawing area)
  fill(0xff00ff00);
  vertex(-5, -5, 40);
  vertex(5, -5, 40);
  vertex(5, 5, 40);
  vertex(-5, 5, 40);
  
  vertex(-40, -5, 5);
  vertex(40, -5, 5);
  vertex(40, 5, 5);
  vertex(-40, 5, 5);
  
  vertex(-10, -5, 10);
  vertex(10, -5, 10);
  vertex(10, 5, 10);
  vertex(-10, 5, 10);

  // z- 
  fill(0xff0000ff);
  vertex(-5, -5, -40);
  vertex(5, -5, -40);
  vertex(5, 5, -40);
  vertex(-5, 5, -40);
  
  vertex(-40, -5, -5);
  vertex(40, -5, -5);
  vertex(40, 5, -5);
  vertex(-40, 5, -5); 
  
  vertex(-10, -5, -10);
  vertex(10, -5, -10);
  vertex(10, 5, -10);
  vertex(-10, 5, -10);
  // x-
  fill(0xffff0000);
  vertex(-5, -5, -40);
  vertex(-5, -5, 40);
  vertex(-5, 5, 40);
  vertex(-5, 5, -40);
  
  vertex(-40, -5, -5);
  vertex(-40, -5, 5);
  vertex(-40, 5, 5);
  vertex(-40, 5, -5);
  
  vertex(-10, -5, -10);
  vertex(-10, -5, 10);
  vertex(-10, 5, 10);
  vertex(-10, 5, -10);
  // x+
  fill(0xffffff00);
  vertex(5, -5, -40);
  vertex(5, -5, 40);
  vertex(5, 5, 40);
  vertex(5, 5, -40);
  
  vertex(40, -5, -5);
  vertex(40, -5, 5);
  vertex(40, 5, 5);
  vertex(40, 5, -5);
  
  vertex(10, -5, -10);
  vertex(10, -5, 10);
  vertex(10, 5, 10);
  vertex(10, 5, -10);
  // y-
  fill(0xffff00ff);
  vertex(-5, -5, -40);
  vertex(5, -5, -40);
  vertex(5, -5, 40);
  vertex(-5, -5, 40);
  
  vertex(-40, -5, -5);
  vertex(40, -5, -5);
  vertex(40, -5, 5);
  vertex(-40, -5, 5);
  
  vertex(-10, -5, -10);
  vertex(10, -5, -10);
  vertex(10, -5, 10);
  vertex(-10, -5, 10);
   
  
  // y+
  fill(0xff00ffff);
  vertex(-5, 5, -40);
  vertex(5, 5, -40);
  vertex(5, 5, 40);
  vertex(-5, 5, 40);
  
  vertex(-40, 5, -5);
  vertex(40, 5, -5);
  vertex(40, 5, 5);
  vertex(-40, 5, 5);
  
  vertex(-10, 5, -10);
  vertex(10, 5, -10);
  vertex(10, 5, 10);
  vertex(-10, 5, 10);
  
  //--- forward_symbol
  fill(0xff17F70C);
  vertex(-5,-10,-40);
  vertex(5,-10,-40);
  vertex(5,-10,-45);
  vertex(-5,-10,-45);
  
  vertex(-5,-5,-40);
  vertex(5,-5,-40);
  vertex(5,-5,-45);
  vertex(-5,-5,-45);
  
  fill(0xff0CF7E6);
  vertex(-5,-10,-40);
  vertex(-5,-10,-45);
  vertex(-5,-5,-45);
  vertex(-5,-5,-40);
  
  vertex(5,-10,-40);
  vertex(5,-10,-45);
  vertex(5,-5,-45);
  vertex(5,-5,-40);
  
  
  fill(0xff0C5CF7);
  vertex(-5,-5,-45);
  vertex(-5,-10,-45); 
  vertex(5,-10,-45);
  vertex(5,-5,-45); 
  
  vertex(-5,-5,-40);
  vertex(-5,-10,-40); 
  vertex(5,-10,-40);
  vertex(5,-5,-40); 
  
  endShape();
  rotateX(-radians(y));
  rotateY(radians(z));
  rotateZ(-radians(x));
   translate(-width/2, -height*2/3-30); 
  //----------------------- end of 3D motion --------------
}

public void setup_text()
{
  txt[0]= "- Author: Doan Trung Nghia - Class: D09DTMT.";
  txt[1]= "- University: Post & Telecommunications Institute of Technology.";
  txt[2]= "- Supervisor: Master Tran Thuc Linh.";
  txt[3]= " ";
  txt[4]=" Some words about my project:";
  txt[5]=" This is my graduated project, in this project i have researched some";
  txt[6]=" electronic issues such as:";
  txt[7]="1. Intertial measurement unit (IMU) with Kalman filter.";
  txt[8]="2. PID controller for quadcopter.";
  txt[9]="3. Programming my own code C/C++ for AVR and ARM micocontroller.";
  txt[10]="4. Design and make a AVR uC PCB for RF data transmit.";
  txt[11]="5. Assemble quadcopter frame and it's components.";
  txt[12]="6. Creat my own GUI to display recived data by using Processing program.";
  txt[13]=" ";
  txt[14]=" I use ATmega 328P, ATmega8 and STM32103ZE uC.";
  txt[15]=" Gyroscope and acclerometer: MPU6050.";
  txt[16]=" Magnetic sensor: HMC5883l and barometer: MS5611.";
  txt[17]=" Please contact me at : doantrungnghia05@gmail.com"; 
}

public void print_txt()
{
  fill(0xff02F720);
  textSize(28);
   for(char i=0;i<18;i++)
   {
   text(txt[i],0,190+i*25);
   }
}

public void draw_temp()
{
   //--------------------------------------------------
  translate(250,650);
  strokeWeight(2);
  stroke(0xffffffff);
  fill(0xff060383);
  rect(0,0,300,15);
  noStroke();
  fill(0xffff0000);
  rect(0,0,temp*6,15);
  strokeWeight(2);
  stroke(0xffffffff);
  for(char i=0;i<6;i++)
  {
    line(60*i,0,60*i,10);
    fill(0xff0AFF0C);
    text(i*10,60*i-10,-10);
  }
  for(char i=0;i<11;i++)
  {
    line(30*i,0,30*i,6);

  }
  
  for(char i=0;i<51;i++)
  {
    line(6*i,0,6*i,3);
  }

  fill(0xff0AFF0C);
  text("oC",310,-10);
  translate(-250,-650);
  //--------------------------------------------------
}
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "my_quad_graphic" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
